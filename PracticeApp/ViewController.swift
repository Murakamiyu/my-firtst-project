//
//  ViewController.swift
//  PracticeApp
//
//  Created by 村上　佑介 on 2019/03/12.
//  Copyright © 2019年 村上　佑介. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let shopLogo = UIImage(named:"mandai")
        imageView.image = shopLogo
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showResult" {
            
        }
    }
    
     ///パスワード忘れに関するイベント
    @IBAction func tapLostPassword(_ sender: UIButton) {
        let testUrl = URL(string: "http://www.mandai-net.co.jp/")
        
        if let testUrl = testUrl {
            let safariViewController = SFSafariViewController(url: testUrl)
            present(safariViewController, animated: false, completion: nil)
        }
    }

    
}

